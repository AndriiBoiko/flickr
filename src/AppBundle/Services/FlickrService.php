<?php

namespace AppBundle\Services;

use AppBundle\Entity\URL;
use AppBundle\Entity\BaseURL;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FlickrService
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var URL
     */
    private $url;

    CONST RECENT_PHOTO_METHOD = 'flickr.photos.getRecent';
    CONST PHOTO_SIZES_METHOD = 'flickr.photos.getSizes';
    CONST FLICKR_API_URL = 'https://api.flickr.com/services/rest/';

    /**
     * FlickrService constructor.
     *
     * @param ContainerInterface $container
     * @param BaseURL $url
     */
    public function __construct(ContainerInterface $container, BaseURL $url)
    {
        $this->container = $container;
        $this->url = $url;
    }

    /**
     * Get recent photos
     *
     * @return array|mixed
     */
    public function getRecentPhotos()
    {
        return $this->container->get('app.curl.service')->getData($this->getURL(self::RECENT_PHOTO_METHOD, null, 'url_c'));
    }

    public function getPhotoSizes($photoId)
    {
        return $this->container->get('app.curl.service')->getData($this->getURL(self::PHOTO_SIZES_METHOD, $photoId));
    }

    private function getURL($flickrMethod, $photoId = null, $extras = null)
    {
        $this->url->setParameter('method', $flickrMethod);
        $this->url->setParameter('api_key', $this->container->getParameter('flickr_api_key'));
        $this->url->setParameter('format', 'json');
        $this->url->setParameter('nojsoncallback', '1');

        if ($photoId) {
            $this->url->setParameter('photo_id', $photoId);
        }

        if ($extras) {
            $this->url->setParameter('extras', $extras);
        }
        return self::FLICKR_API_URL . '?' . $this->url->getFullUrl();
    }
}