<?php

namespace AppBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;

class CurlService
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * CurlService constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getData($url)
    {
        //init curl
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        //get response
        $response = curl_exec($ch);

        //check content type of response
        $contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
        if($contentType != 'application/json') {
            return [];
        }

        //close curl
        curl_close($ch);

        //return response
        return json_decode($response, true);
    }
}