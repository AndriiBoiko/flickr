<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends FOSRestController
{
    /**
     * Send response
     *
     * @param $data
     * @param int $statusCode
     * @return \FOS\RestBundle\View\View
     */
    public function sendResponse($data, $statusCode = Response::HTTP_OK)
    {

        if (empty($data)) {
            return $this->sendEmptyResponse();
        }

        return $this->view($data, $statusCode);
    }


    /**
     * Send not found response
     *
     * @param int $statusCode
     * @return \FOS\RestBundle\View\View
     */
    public function sendEmptyResponse($statusCode = Response::HTTP_NOT_FOUND)
    {
        return $this->view(['data' => 'Not found!'], $statusCode);

    }
}