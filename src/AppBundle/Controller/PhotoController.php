<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations;

class PhotoController extends BaseController
{
    /**
     * Get recent photos
     *
     * @Annotations\Get("/api/v1/photos", name="api_v1_get_recent_photos")
     *
     * @return Annotations\View
     */
    public function getRecentPhotosAction()
    {
        //get recent photos
        $photos = $this->get('app.flickr.service')->getRecentPhotos();
        return $this->sendResponse($photos);
    }

    /**
     * Returns the available sizes for a photo.
     *
     * @Annotations\Get("/api/v1/photos/{photoId}/sizes", name="api_v1_get_photo_sizes")
     *
     * @param $photoId
     * @return Annotations\View
     */
    public function getPhotoSizesAction($photoId)
    {
        //get photo sizes
        $photoSizes = $this->get('app.flickr.service')->getPhotoSizes((int)$photoId);
        return $this->sendResponse($photoSizes);
    }
}