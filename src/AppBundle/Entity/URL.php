<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class URL extends BaseURL
{
    private $parameters;

    public function __construct()
    {
        $this->parameters = [];
    }

    /** @return string */
    public function getFullUrl()
    {
        return http_build_query($this->parameters);
    }

    public function setParameter($key, $value)
    {
        $this->parameters[$key] = $value;
    }
}