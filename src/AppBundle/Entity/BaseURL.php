<?php

namespace AppBundle\Entity;

abstract class BaseURL
{
    /** @return string */
    abstract public function getFullURL();

    abstract public function setParameter($key, $value);
}